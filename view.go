package Alpaca

import (
    "html/template"
    "bytes"
    "fmt"
)

type View struct {
    Path string
}

/*
func (vw *View) getTemplate(tpl []string) (*template.Template, error) {
	var (
		tmpl *template.Template
		err error
		fp []string = make([]string,len(tpl))
    )

		for i, t := range tpl {
			fp[i] = path.Join(vw.Path, t)
		}
		tmpl = template.New(path.Base(tpl[0]))
		tmpl, err = template.ParseFiles(fp...)
		if err != nil {
			return nil, err
		}
		return tmpl, nil
}
*/

func (vw *View) Render(tpl string, data map[string]interface{}) ([]byte, error) {
    var err error
    tmpl := template.New(tpl)
    tmpl, err = tmpl.ParseFiles(tpl)
    if err != nil {
        fmt.Println(err)
        return nil, err
    }

    fmt.Println(tmpl)
	var buf bytes.Buffer
	//err = tmpl.ExecuteTemplate(&buf, name, nil)
	err = tmpl.Execute(&buf, data)
	if err != nil {
        fmt.Println(err)
		return nil, err
	}
	return buf.Bytes(), nil
}


func NewView(path string) *View {
    v := new(View)
    v.Path = path

    return v
}
