package Alpaca

import (
    "strings"
    "regexp"
)


type RouteContainer struct {
    routeMap []*Router
}

type Router struct {
	reg    *regexp.Regexp
	method    string
	params    []string
	hd    []Handler
}

type Handler func(ctx *Context)

type RouterCache struct {
	params map[string]string
	hd []Handler
}


func NewRouterContainer() *RouteContainer {
    rc := new(RouteContainer)
    rc.routeMap = make([]*Router, 0)

    return rc
}


func NewRouter() *Router {
    rt := new(Router)
    rt.params = make([]string, 0)

    return rt
}


func (rc *RouteContainer) Regist(mt string, pattern string, hd []Handler) {
	rt := NewRouter()
	rt.reg, rt.params = rc.patternParse(pattern)
	rt.method = mt
	rt.hd = hd
	rc.routeMap = append(rc.routeMap, rt)
}


func (rc *RouteContainer) patternParse(pattern string) (reg *regexp.Regexp, params []string) {
    params = make([]string, 0)
	parts := strings.Split(pattern, "/")
	for k, v := range parts {
		if strings.HasPrefix(v, "<"); strings.HasSuffix(v, ">") {
			parts[k] = `([^/])`
			params = append(params, v)
		}
	}

	reg, _ = regexp.Compile("^" + strings.Join(parts, "/") + "$")
	return
}

func (rc *RouteContainer) Search(url string, method string) (params map[string]string, hd []Handler) {
	//if !strings.HasSuffix(url, "/") {
//		url += "/"
//	}

	for _, rt := range rc.routeMap {
		if rt.reg.MatchString(url) && rt.method == method {
			p := rt.reg.FindStringSubmatch(url)
			if len(p) != len(rt.params)+1 {
				continue
			}
			params = make(map[string]string)
			for k, v := range rt.params {
				params[v] = p[k+1]
			}
			hd = rt.hd
			return
		}
	}
	return nil, nil
}


