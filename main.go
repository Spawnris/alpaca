package main

import "Alpaca"
import "fmt"

// default handler, implement GoInk.Handler
func homeHandler(ctx *Alpaca.Context) {
	ctx.Body = []byte("Hello Alpaca!")
    //fmt.Println(ctx.Get("<id>"))
    ctx.Body = []byte(ctx.Get("<id>"))

}

func testHandler(ctx *Alpaca.Context) {
    fmt.Println("test1")
    test := make(map[string]interface{})
    test["one"] = "two"
    data, err := ctx.Render("test.html", test)
    if err != nil {
//``        fmt.Println(err)
    }
    fmt.Println(data)
	ctx.Body = []byte(data)
}

func main() {
	// only bind GET handler by homeHandler.
	// if other http method, return 404.
    app := Alpaca.New()
    app.Get("/<id>", homeHandler)
    app.Get("/test", testHandler)
    app.Test()
    app.Run()
}
