package Alpaca

import (
    "fmt"
    "net/http"
//    "strings"
	"log"
)

type App struct {
    rc  *RouteContainer
    cache map[string]*RouterCache
    view *View
}

func New() *App {
	ap := new(App)
	ap.rc = NewRouterContainer()
    ap.cache = make(map[string]*RouterCache)
    ap.view = NewView("template")
	return ap
}


func (ap *App) Run() {
    println("Server is staring...")
	service := http.ListenAndServe(":8765", ap)
	log.Fatal(service)
}


func (ap *App) Get(path string, hd ...Handler) {
	ap.rc.Regist("GET", path, hd)
}


func (ap *App) Post(path string, hd ...Handler) {
	ap.rc.Regist("POST", path, hd)
}


func (ap *App) Put(path string, hd ...Handler) {
	ap.rc.Regist("PUT", path, hd)
}


func (ap *App) DELETE(path string, hd ...Handler) {
	ap.rc.Regist("DELETE", path, hd)
}

func (ap *App) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	ctx := NewContext(ap, rep, req)

	defer func() {
		if e := recover(); e != nil {
			ctx.Body = []byte(fmt.Sprint(e))
			ctx.Status = 503
		}
	}()

    var (
        params map[string]string
        hd []Handler
        url = req.URL.Path
    )

    if _, ok := ap.cache[url]; ok {
        params = ap.cache[url].params
        hd = ap.cache[url].hd
    } else {
        params, hd = ap.rc.Search(url, req.Method)
    }
    if params != nil && hd != nil {
        ctx.params = params

        ca := new(RouterCache)
        ca.params = params
        ca.hd = hd
        ap.cache[url] = ca

        for _, hand := range hd {
            hand(ctx)
            if ctx.Sign == SIGN_DONE {
                break
            }
        }
        if ctx.Sign != SIGN_DONE {
            ctx.Done()
        }
    }
    ctx = nil
}

func (ap *App) Test() {
    for k, v := range(ap.rc.routeMap) {
        fmt.Println(k)
        fmt.Println(v)
    }
}

