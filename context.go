package Alpaca

import (
	"net/http"
	"strings"
)

const (
    SIGN_REND = 1
    SIGN_RESP = 2
    SIGN_DONE = 3
)

type Context struct {
	ap     *App
	params map[string]string

	Req        *http.Request
	Url        string
	RequestUrl string
	Method     string
	Ip         string
	UserAgent  string
	Refer      string
	Host       string

	Status int
	Header map[string]string
	Rep    http.ResponseWriter
	Body   []byte

    Sign    int
}


func NewContext(ap *App, rep http.ResponseWriter, req *http.Request) *Context {
	ctx := new(Context)
	ctx.ap = ap

	ctx.Req = req
	ctx.Url = req.URL.Path
	ctx.Method = req.Method
	ctx.Host = req.Host
	ctx.RequestUrl = req.RequestURI
	ctx.Refer = req.Referer()
	ctx.UserAgent = req.UserAgent()
	ctx.Ip = strings.Split(req.RemoteAddr, ":")[0]

	ctx.Rep = rep
	ctx.Status = 200
	ctx.Header = make(map[string]string)
    ctx.Sign = 0

//	req.ParserForm()

	return ctx
}

func (ctx *Context) Get(key string) string {
    return ctx.params[key]
}

func (ctx *Context) Done() {
    if ctx.Sign == SIGN_DONE {
        return
    }
    if ctx.Sign != SIGN_RESP {
        ctx.Resp()
    }
    ctx.Sign = SIGN_DONE
}


func (ctx *Context) Resp() {
    if ctx.Sign == SIGN_DONE {
        return
    }
    for k, v := range ctx.Header {
       ctx.Rep.Header().Set(k, v)
    }
    ctx.Rep.WriteHeader(ctx.Status)
    ctx.Rep.Write(ctx.Body)
    ctx.Sign = SIGN_RESP
}


func (ctx *Context) Render(tmpl string, data map[string]interface{}) ([]byte, error){
    t, err := ctx.ap.view.Render(tmpl, data)
    if err != nil {
        return nil, err
    }
    return t, nil
}


func (ctx *Context) App() *App {
    return ctx.ap
}
